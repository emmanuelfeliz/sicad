/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.dao;

import br.com.nastek.model.Aluno;
import br.com.nastek.util.CriadorDeSession;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author emmanuel.oliveira
 */
@RequestScoped
public class AlunoDao {

    private final Session session;

    public AlunoDao() {
        this.session = CriadorDeSession.getSession();

    }

    public void salva(Aluno aluno) {

        Transaction tx = session.beginTransaction();
        session.save(aluno);
        tx.commit();
    }

    public void atualiza(Aluno aluno) {

        Transaction tx = session.beginTransaction();
        session.update(aluno);
        tx.commit();
    }

    public void deletar(Aluno aluno) {

        Transaction tx = session.beginTransaction();
        session.delete(aluno);
        tx.commit();

    }

    public List<Aluno> listarTodos() {
        return session.createCriteria(Aluno.class).addOrder(Order.asc("id")).list();
    }

    public Aluno findById(Long id) {
        return (Aluno) session.load(Aluno.class, id);
    }

}
