/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.dao;

import br.com.nastek.model.Avaliacao;
import br.com.nastek.util.CriadorDeSession;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author emmanuel.oliveira
 */
@RequestScoped
public class AvaliacaoDao {

    private final Session session;

    public AvaliacaoDao() {
        this.session = CriadorDeSession.getSession();

    }

    public void salva(Avaliacao avaliacao) {

        Transaction tx = session.beginTransaction();
        session.save(avaliacao);
        tx.commit();
    }

    public void atualiza(Avaliacao avaliacao) {

        Transaction tx = session.beginTransaction();
        session.update(avaliacao);
        tx.commit();
    }

    public void deletar(Avaliacao avaliacao) {

        Transaction tx = session.beginTransaction();
        session.delete(avaliacao);
        tx.commit();

    }

    public List<Avaliacao> listarTodos() {
        return session.createCriteria(Avaliacao.class).addOrder(Order.asc("id")).list();
    }

    public Avaliacao findById(Long id) {
        return (Avaliacao) session.load(Avaliacao.class, id);
    }

}
