/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.dao;

import br.com.nastek.model.Curso;
import br.com.nastek.util.CriadorDeSession;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author emmanuel.oliveira
 */
@RequestScoped
public class CursoDao {

    private final Session session;

    public CursoDao() {
        this.session = CriadorDeSession.getSession();

    }

    public void salva(Curso curso) {

        Transaction tx = session.beginTransaction();
        session.save(curso);
        tx.commit();
    }

    public void atualiza(Curso curso) {

        Transaction tx = session.beginTransaction();
        session.update(curso);
        tx.commit();
    }

    public void deletar(Curso curso) {

        Transaction tx = session.beginTransaction();
        session.delete(curso);
        tx.commit();

    }

    public List<Curso> listarTodos() {
        return session.createCriteria(Curso.class).addOrder(Order.asc("id")).list();
    }

    public Curso findById(Long id) {
        return (Curso) session.load(Curso.class, id);
    }

}
