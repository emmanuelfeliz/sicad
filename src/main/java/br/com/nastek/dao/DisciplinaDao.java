/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.dao;

import br.com.nastek.model.Disciplina;
import br.com.nastek.util.CriadorDeSession;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author emmanuel.oliveira
 */
@RequestScoped
public class DisciplinaDao {

    private final Session session;

    public DisciplinaDao() {
        this.session = CriadorDeSession.getSession();

    }

    public void salva(Disciplina disciplina) {

        Transaction tx = session.beginTransaction();
        session.save(disciplina);
        tx.commit();
    }

    public void atualiza(Disciplina disciplina) {

        Transaction tx = session.beginTransaction();
        session.update(disciplina);
        tx.commit();
    }

    public void deletar(Disciplina disciplina) {

        Transaction tx = session.beginTransaction();
        session.delete(disciplina);
        tx.commit();

    }

    public List<Disciplina> listarTodos() {
        return session.createCriteria(Disciplina.class).addOrder(Order.asc("id")).list();
    }

    public Disciplina findById(Long id) {
        return (Disciplina) session.load(Disciplina.class, id);
    }

}
