/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.dao;

import br.com.nastek.model.Pessoa;
import br.com.nastek.util.CriadorDeSession;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author emmanuel.oliveira
 */
@RequestScoped
public class PessoaDao {

    private final Session session;

    public PessoaDao() {
        this.session = CriadorDeSession.getSession();

    }

    public void salva(Pessoa pessoa) {

        Transaction tx = session.beginTransaction();
        session.save(pessoa);
        tx.commit();
    }

    public void atualiza(Pessoa pessoa) {

        Transaction tx = session.beginTransaction();
        session.update(pessoa);
        tx.commit();
    }

    public void deletar(Pessoa pessoa) {

        Transaction tx = session.beginTransaction();
        session.delete(pessoa);
        tx.commit();

    }

    public List<Pessoa> listarTodos() {
        return session.createCriteria(Pessoa.class).addOrder(Order.asc("id")).list();
    }

    public Pessoa findById(Long id) {
        return (Pessoa) session.load(Pessoa.class, id);
    }

}
