/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.dao;

import br.com.nastek.model.Professor;
import br.com.nastek.util.CriadorDeSession;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author emmanuel.oliveira
 */
@RequestScoped
public class ProfessorDao {

    private final Session session;

    public ProfessorDao() {
        this.session = CriadorDeSession.getSession();

    }

    public void salva(Professor professor) {

        Transaction tx = session.beginTransaction();
        session.save(professor);
        tx.commit();
    }

    public void atualiza(Professor professor) {

        Transaction tx = session.beginTransaction();
        session.update(professor);
        tx.commit();
    }

    public void deletar(Professor professor) {

        Transaction tx = session.beginTransaction();
        session.delete(professor);
        tx.commit();

    }

    public List<Professor> listarTodos() {
        return session.createCriteria(Professor.class).addOrder(Order.asc("id")).list();
    }

    public Professor findById(Long id) {
        return (Professor) session.load(Professor.class, id);
    }

}
