/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.dao;

import br.com.nastek.model.Turma;
import br.com.nastek.util.CriadorDeSession;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author emmanuel.oliveira
 */
@RequestScoped
public class TurmaDao {

    private final Session session;

    public TurmaDao() {
        this.session = CriadorDeSession.getSession();

    }

    public void salva(Turma turma) {

        Transaction tx = session.beginTransaction();
        session.save(turma);
        tx.commit();
    }

    public void atualiza(Turma turma) {

        Transaction tx = session.beginTransaction();
        session.update(turma);
        tx.commit();
    }

    public void deletar(Turma turma) {

        Transaction tx = session.beginTransaction();
        session.delete(turma);
        tx.commit();

    }

    public List<Turma> listarTodos() {
        return session.createCriteria(Turma.class).addOrder(Order.asc("id")).list();
    }

    public Turma findById(Long id) {
        return (Turma) session.load(Turma.class, id);
    }

}
