/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.model;

import javax.persistence.Entity;

/**
 *
 * @author emmanuel.oliveira
 */

@Entity
public class Aluno extends Pessoa{
    
    private String matricula;
    private String situacao;

    public Aluno() {
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    
    public void matricularCurso(){
        
    }
    
    public void trancar(){
        
    }
    
    public void formar(){
        
    }
    
    public void evadir(){
        
    }
    
    public void obterAvaliacoes(){
        
    }
    
    public void emitirHistorico(){
        
    }
    
}
