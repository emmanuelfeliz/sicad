/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

/**
 *
 * @author emmanuel.oliveira
 */

@Entity
public class Disciplina {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToMany 
    @JoinColumn (name="idpreRequisito", insertable = true, updatable = true, nullable = true) 
    private List<Disciplina> preRequisito = new ArrayList<>();
    
    private Integer codigo;
    private String  descricao;
    private Integer cargaHoraria;
    private String  ementa;
    private String  bibliografia;

    public Disciplina() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Disciplina> getPreRequisito() {
        return preRequisito;
    }

    public void setPreRequisito(List<Disciplina> preRequisito) {
        this.preRequisito = preRequisito;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(Integer cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(String bibliografia) {
        this.bibliografia = bibliografia;
    }

    
    
}
