/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.model;

import javax.persistence.Entity;

/**
 *
 * @author emmanuel.oliveira
 */
@Entity
public class Professor extends Pessoa {
    
    private String titulacaoMaxima;    

    public Professor() {
    }

    public String getTitulacaoMaxima() {
        return titulacaoMaxima;
    }

    public void setTitulacaoMaxima(String titulacaoMaxima) {
        this.titulacaoMaxima = titulacaoMaxima;
    }    
    
}
