/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.test;

import br.com.nastek.dao.AvaliacaoDao;
import br.com.nastek.model.Avaliacao;
import javax.swing.JOptionPane;

/**
 *
 * @author emmanuel.oliveira
 */
public class AvaliacaoTest {

    public static void main(String[] args) {

        Integer opcao = 5;

        while (opcao != 0) {

            opcao = Integer.parseInt(JOptionPane.showInputDialog("\n"
                    + "Choose your operation:\n"
                    + "[1] Save a new register\n"
                    + "[2] Update a register\n"
                    + "[3] Delete a register\n"
                    + "[4] Find a register by id\n"
                    + "[5] List all registers\n"));

            switch (opcao) {
                case 0:
                    opcao = 0;
                    break;
                case 1:
                    salvar();
                    break;
                case 2:
                    atualiza();
                    break;
                case 3:
                    deletar();
                    break;
                case 4:
                    System.out.println("Not implemented!");
                    break;
                case 5:
                    System.out.println("Not implemented!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Type a valid option!");
                    break;
            }

        }

    }

    private static void salvar() {

        Avaliacao avaliacao = criaAvaliacao();
        AvaliacaoDao avaliacaoDao = new AvaliacaoDao();
        avaliacaoDao.salva(avaliacao);

    }

    private static Avaliacao criaAvaliacao() {
        Avaliacao avaliacao = new Avaliacao();
        avaliacao.setNota1(10.0f);
        return avaliacao;
    }

    private static void atualiza() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        AvaliacaoDao avaliacaoDao = new AvaliacaoDao();
        Avaliacao avaliacaoD = avaliacaoDao.findById(id);
        avaliacaoD.setNota1(5.0f);
        avaliacaoD.setNota2(10.0f);
        avaliacaoD.setFrequencia(5);
        avaliacaoDao.atualiza(avaliacaoD);
    }

    private static void deletar() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        AvaliacaoDao avaliacaoDao = new AvaliacaoDao();
        Avaliacao avaliacaoD = avaliacaoDao.findById(id);
        avaliacaoDao.deletar(avaliacaoD);

    }

}
