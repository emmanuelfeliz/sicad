/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.test;

import br.com.nastek.dao.AlunoDao;
import br.com.nastek.dao.CursoDao;
import br.com.nastek.dao.ProfessorDao;
import br.com.nastek.model.Aluno;
import br.com.nastek.model.Curso;
import br.com.nastek.model.Professor;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author emmanuel.oliveira
 */
public class CursoTest {
    
    public static void main(String[] args) {
     
        
        Integer opcao = 5;

        while (opcao != 0) {

            opcao = Integer.parseInt(JOptionPane.showInputDialog("\n"
                    + "Choose your operation:\n"
                    + "[1] Save a new register\n"
                    + "[2] Update a register\n"
                    + "[3] Delete a register\n"
                    + "[4] Find a register by id\n"
                    + "[5] List all registers\n"
                    + "[6] Insert a new schoolboy\n"
                    + "[7] Insert a new teacher\n"));

            switch (opcao) {
                case 0:
                    opcao = 0;
                    break;
                case 1:
                    salvar();
                    break;
                case 2:
                    atualiza();
                    break;
                case 3:
                    deletar();
                    break;
                case 4:
                    System.out.println("Not implemented!");
                    break;
                case 5:
                    System.out.println("Not implemented!");
                    break;
                case 6:
                    inserirAluno();
                    break;
                case 7:
                    inserirProfessor();
                default:
                    JOptionPane.showMessageDialog(null, "Type a valid option!");
                    break;
            }

        }
               
    }
   
    private static void salvar(){
    
        Curso curso = criaCurso();
        
        CursoDao cursoDao = new CursoDao();
        cursoDao.salva(curso);
        
    }
    
    
    private static Curso criaCurso() {
        Curso curso = new Curso();
        curso.setCodigo(1);
        curso.setDescricao("Administração");
        return curso;
    }
       
    private static void atualiza(){
        
        Long id;
        
        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));
        
        CursoDao cursoDao = new CursoDao();
        Curso cursoD = cursoDao.findById(id);
        cursoD.setDescricao("Novo");
        cursoDao.atualiza(cursoD);
    }
    
    private static void deletar(){
        
        Long id;       
        
        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));
        
        CursoDao cursoDao = new CursoDao();
        Curso cursoD = cursoDao.findById(id);
        cursoDao.deletar(cursoD);
        
    }
    
    private static void inserirAluno(){
        
        Long idCurso = Long.parseLong(JOptionPane.showInputDialog("Type course id"));
        
        Long idAluno = Long.parseLong(JOptionPane.showInputDialog("Type schoolboy id"));

        CursoDao cursoDao = new CursoDao();
        Curso curso = cursoDao.findById(idCurso);
        
        AlunoDao alunoDao = new AlunoDao();
        Aluno aluno = alunoDao.findById(idAluno);
        
        ArrayList<Aluno> alunosList = new ArrayList<>();
        alunosList.add(aluno);
        
        curso.setAluno(alunosList);
        cursoDao.atualiza(curso);
        
    }
    
    private static void inserirProfessor(){
        
        Long idCurso = Long.parseLong(JOptionPane.showInputDialog("Type course id"));
        
        Long idProfessor = Long.parseLong(JOptionPane.showInputDialog("Type teacher id"));

        CursoDao cursoDao = new CursoDao();
        Curso curso = cursoDao.findById(idCurso);
        
        ProfessorDao professorDao = new ProfessorDao();
        Professor professor = professorDao.findById(idProfessor);
        
        ArrayList<Professor> professoresList = new ArrayList<>();
        professoresList.add(professor);
        
        curso.setProfessor(professoresList);
        cursoDao.atualiza(curso);
        
    }


}
