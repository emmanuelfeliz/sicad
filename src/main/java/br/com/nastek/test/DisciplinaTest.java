/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.test;

import br.com.nastek.dao.DisciplinaDao;
import br.com.nastek.model.Disciplina;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author emmanuel.oliveira
 */
public class DisciplinaTest {

    public static void main(String[] args) {

        Integer opcao = 5;

        while (opcao != 0) {

            opcao = Integer.parseInt(JOptionPane.showInputDialog("\n"
                    + "Choose your operation:\n"
                    + "[1] Save a new register\n"
                    + "[2] Update a register\n"
                    + "[3] Delete a register\n"
                    + "[4] Find a register by id\n"
                    + "[5] List all registers\n"));

            switch (opcao) {
                case 0:
                    opcao = 0;
                    break;
                case 1:
                    salvar();
                    break;
                case 2:
                    atualiza();
                    break;
                case 3:
                    deletar();
                    break;
                case 4:
                    System.out.println("Not implemented!");
                    break;
                case 5:
                    System.out.println("Not implemented!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Type a valid option!");
                    break;
            }

        }

    }

    private static void salvar() {

        Disciplina disciplina = criaDisciplina();
        DisciplinaDao disciplinaDao = new DisciplinaDao();
        disciplinaDao.salva(disciplina);

    }

    private static Disciplina criaDisciplina() {
        Disciplina disciplina = new Disciplina();
        
        disciplina.setBibliografia("Livros 1");
        disciplina.setCargaHoraria(120);
        disciplina.setCodigo(12312);
        disciplina.setDescricao("Matematica");
        disciplina.setEmenta("conteudo");
        
        Disciplina disciplina2 = new Disciplina();
        disciplina2.setDescricao("MatematicaElementar");
        disciplina2.setCargaHoraria(120);
        disciplina2.setCodigo(12311);
        disciplina2.setDescricao("Matematica");
        disciplina2.setEmenta("conteudo");
        
        DisciplinaDao disciplinaDao = new DisciplinaDao();
        disciplinaDao.salva(disciplina2);
        
        
        ArrayList<Disciplina> disciplinaList = new ArrayList<>();
        disciplinaList.add(disciplina2);
        
        disciplina.setPreRequisito(disciplinaList);
        return disciplina;
    }

    private static void atualiza() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        DisciplinaDao disciplinaDao = new DisciplinaDao();
        Disciplina disciplinaD = disciplinaDao.findById(id);
        disciplinaD.setCargaHoraria(150);
        disciplinaDao.atualiza(disciplinaD);
    }

    private static void deletar() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        DisciplinaDao disciplinaDao = new DisciplinaDao();
        Disciplina disciplinaD = disciplinaDao.findById(id);
        disciplinaDao.deletar(disciplinaD);

    }

}
