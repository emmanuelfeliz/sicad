/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.test;

import br.com.nastek.dao.PessoaDao;
import br.com.nastek.model.Pessoa;
import javax.swing.JOptionPane;

/**
 *
 * @author emmanuel.oliveira
 */
public class PessoaTest {

    public static void main(String[] args) {

        Integer opcao = 5;

        while (opcao != 0) {

            opcao = Integer.parseInt(JOptionPane.showInputDialog("\n"
                    + "Choose your operation:\n"
                    + "[1] Save a new register\n"
                    + "[2] Update a register\n"
                    + "[3] Delete a register\n"
                    + "[4] Find a register by id\n"
                    + "[5] List all registers\n"));

            switch (opcao) {
                case 0:
                    opcao = 0;
                    break;
                case 1:
                    salvar();
                    break;
                case 2:
                    atualiza();
                    break;
                case 3:
                    deletar();
                    break;
                case 4:
                    System.out.println("Not implemented!");
                    break;
                case 5:
                    System.out.println("Not implemented!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Type a valid option!");
                    break;
            }

        }

    }

    private static void salvar() {

        Pessoa pessoa = criaPessoa();
        PessoaDao pessoaDao = new PessoaDao();
        pessoaDao.salva(pessoa);

    }

    private static Pessoa criaPessoa() {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Rafaela");
        pessoa.setEndereco("Rua dos anjos, 1001");
        pessoa.setTelefone("9093-0065");
        return pessoa;
    }

    private static void atualiza() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        PessoaDao pessoaDao = new PessoaDao();
        Pessoa pessoaD = pessoaDao.findById(id);
        pessoaD.setEndereco("Rua anjo, 1365");
        pessoaDao.atualiza(pessoaD);
    }

    private static void deletar() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        PessoaDao pessoaDao = new PessoaDao();
        Pessoa pessoaD = pessoaDao.findById(id);
        pessoaDao.deletar(pessoaD);

    }

}
