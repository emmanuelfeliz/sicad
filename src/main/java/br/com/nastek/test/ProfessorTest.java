/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.test;

import br.com.nastek.dao.ProfessorDao;
import br.com.nastek.model.Professor;
import javax.swing.JOptionPane;

/**
 *
 * @author emmanuel.oliveira
 */
public class ProfessorTest {

    public static void main(String[] args) {

        Integer opcao = 5;

        while (opcao != 0) {

            opcao = Integer.parseInt(JOptionPane.showInputDialog("\n"
                    + "Choose your operation:\n"
                    + "[1] Save a new register\n"
                    + "[2] Update a register\n"
                    + "[3] Delete a register\n"
                    + "[4] Find a register by id\n"
                    + "[5] List all registers\n"));

            switch (opcao) {
                case 0:
                    opcao = 0;
                    break;
                case 1:
                    salvar();
                    break;
                case 2:
                    atualiza();
                    break;
                case 3:
                    deletar();
                    break;
                case 4:
                    System.out.println("Not implemented!");
                    break;
                case 5:
                    System.out.println("Not implemented!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Type a valid option!");
                    break;
            }

        }

    }

    private static void salvar() {

        Professor professor = criaProfessor();
        ProfessorDao professorDao = new ProfessorDao();
        professorDao.salva(professor);

    }

    private static Professor criaProfessor() {
        Professor professor = new Professor();
        professor.setTitulacaoMaxima("Phd.");
        professor.setEndereco("R. Claudia, 178");
        
        professor.setNome("Angela");
        professor.setTelefone("9093-6500");
        return professor;
    }

    private static void atualiza() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        ProfessorDao professorDao = new ProfessorDao();
        Professor professorD = professorDao.findById(id);
        professorD.setTitulacaoMaxima("phd");
        professorDao.atualiza(professorD);
    }

    private static void deletar() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        ProfessorDao professorDao = new ProfessorDao();
        Professor professorD = professorDao.findById(id);
        professorDao.deletar(professorD);

    }

}
