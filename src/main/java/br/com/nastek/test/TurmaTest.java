/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.test;

import br.com.nastek.dao.TurmaDao;
import br.com.nastek.model.Turma;
import javax.swing.JOptionPane;

/**
 *
 * @author emmanuel.oliveira
 */
public class TurmaTest {

    public static void main(String[] args) {

        Integer opcao = 5;

        while (opcao != 0) {

            opcao = Integer.parseInt(JOptionPane.showInputDialog("\n"
                    + "Choose your operation:\n"
                    + "[1] Save a new register\n"
                    + "[2] Update a register\n"
                    + "[3] Delete a register\n"
                    + "[4] Find a register by id\n"
                    + "[5] List all registers\n"));

            switch (opcao) {
                case 0:
                    opcao = 0;
                    break;
                case 1:
                    salvar();
                    break;
                case 2:
                    atualiza();
                    break;
                case 3:
                    deletar();
                    break;
                case 4:
                    System.out.println("Not implemented!");
                    break;
                case 5:
                    System.out.println("Not implemented!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Type a valid option!");
                    break;
            }

        }

    }

    private static void salvar() {

        Turma turma = criaTurma();
        TurmaDao turmaDao = new TurmaDao();
        turmaDao.salva(turma);

    }

    private static Turma criaTurma() {
        Turma turma = new Turma();
        turma.setAno(2018);
        turma.setDiaSemana(1);
        turma.setHorarios("19:00-20:00");
        
        turma.setSemestre(1);
        return turma;
    }

    private static void atualiza() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        TurmaDao turmaDao = new TurmaDao();
        Turma turmaD = turmaDao.findById(id);
        turmaD.setHorarios("19:30-21:00");
        turmaDao.atualiza(turmaD);
    }

    private static void deletar() {

        Long id;

        id = Long.parseLong(JOptionPane.showInputDialog("Plese type the value id."));

        TurmaDao turmaDao = new TurmaDao();
        Turma turmaD = turmaDao.findById(id);
        turmaDao.deletar(turmaD);

    }

}
