/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nastek.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author emmanuel.oliveira
 */
public class CriadorDeSession {

    public static Session getSession() throws HibernateException {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        return session;
    }
    
}
